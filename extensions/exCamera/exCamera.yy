{
    "id": "6deab478-2c7e-4224-b675-358766c54ea9",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "exCamera",
    "IncludedResources": [
        "Sprites\\exCamera_DEMO\\Markers\\spr_ex_camera_demo_marker_bounds",
        "Sprites\\exCamera_DEMO\\Markers\\spr_ex_camera_demo_marker_point",
        "Sprites\\exCamera_DEMO\\Markers\\spr_ex_camera_demo_marker_object",
        "Sprites\\exCamera_DEMO\\spr_ex_camera_demo_player",
        "Scripts\\exCamera_DEMO\\scr_test_focus_player",
        "Scripts\\Easing\\scr_ease_in_back",
        "Scripts\\Easing\\scr_ease_in_bounce",
        "Scripts\\Easing\\scr_ease_in_circ",
        "Scripts\\Easing\\scr_ease_in_cubic",
        "Scripts\\Easing\\scr_ease_in_elastic",
        "Scripts\\Easing\\scr_ease_in_expo",
        "Scripts\\Easing\\scr_ease_in_quad",
        "Scripts\\Easing\\scr_ease_in_quart",
        "Scripts\\Easing\\scr_ease_in_quint",
        "Scripts\\Easing\\scr_ease_in_sine",
        "Scripts\\Easing\\scr_ease_inout_back",
        "Scripts\\Easing\\scr_ease_inout_bounce",
        "Scripts\\Easing\\scr_ease_inout_circ",
        "Scripts\\Easing\\scr_ease_inout_cubic",
        "Scripts\\Easing\\scr_ease_inout_elastic",
        "Scripts\\Easing\\scr_ease_inout_expo",
        "Scripts\\Easing\\scr_ease_inout_quad",
        "Scripts\\Easing\\scr_ease_inout_quart",
        "Scripts\\Easing\\scr_ease_inout_quint",
        "Scripts\\Easing\\scr_ease_inout_sine",
        "Scripts\\Easing\\scr_ease_linear",
        "Scripts\\Easing\\scr_ease_out_back",
        "Scripts\\Easing\\scr_ease_out_bounce",
        "Scripts\\Easing\\scr_ease_out_circ",
        "Scripts\\Easing\\scr_ease_out_cubic",
        "Scripts\\Easing\\scr_ease_out_elastic",
        "Scripts\\Easing\\scr_ease_out_expo",
        "Scripts\\Easing\\scr_ease_out_quad",
        "Scripts\\Easing\\scr_ease_out_quart",
        "Scripts\\Easing\\scr_ease_out_quint",
        "Scripts\\Easing\\scr_ease_out_sine",
        "Scripts\\Easing\\scr_ease_slowmo",
        "Scripts\\Easing\\scr_ease_step",
        "Fonts\\exCamera_DEMO\\fnt_ex_camera_test",
        "Objects\\obj_ex_camera",
        "Objects\\exCamera_DEMO\\Markers\\obj_ex_camera_demo_marker_point",
        "Objects\\exCamera_DEMO\\Markers\\obj_ex_camera_demo_marker_object",
        "Objects\\exCamera_DEMO\\Markers\\obj_ex_camera_demo_marker_bounds",
        "Objects\\exCamera_DEMO\\obj_ex_camera_demo_camera",
        "Objects\\exCamera_DEMO\\obj_ex_camera_demo_player",
        "Rooms\\exCamera_DEMO\\rm_ex_camera_demo"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 35223026794734,
    "date": "2017-25-31 09:08:11",
    "description": "exCamera allows you for cameras with simple movement and effects",
    "extensionName": "",
    "files": [
        {
            "id": "53111588-6d50-4d7d-9238-15a45cf99432",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                {
                    "id": "33fb453c-b11b-438a-b4c4-0f0b5d3742ee",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "ex_camera_arguments_undefined",
                    "hidden": false,
                    "value": "\"__ARGUMENTS_UNDEFINED__\""
                },
                {
                    "id": "7bcd7ce0-8777-41d4-9c2c-08694f5ce67f",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "ex_camera_draw_target_event",
                    "hidden": false,
                    "value": "0"
                },
                {
                    "id": "57e4d947-cba7-4751-97b8-362af9b43136",
                    "modelName": "GMExtensionConstant",
                    "mvc": "1.0",
                    "constantName": "ex_camera_draw_target_gui",
                    "hidden": false,
                    "value": "1"
                }
            ],
            "copyToTargets": 9223372036854775807,
            "filename": "exCamera.gml",
            "final": "",
            "functions": [
                {
                    "id": "4cf90769-8a81-4fc9-8513-ea1a1d26e582",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_clear_bounds",
                    "help": "ex_camera_clear_bounds(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_clear_bounds",
                    "returnType": 2
                },
                {
                    "id": "69fcd511-9d68-49eb-a231-649b17735b5a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_count",
                    "help": "ex_camera_count()",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_count",
                    "returnType": 2
                },
                {
                    "id": "683a9808-53b7-4cb9-8627-ed21cf7badde",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_create",
                    "help": "ex_camera_create(name, view, x, y, width, height, scrollX, scrollY, angle, zoom, focusInstance, limitBounds, boundX, boundY, boundWidth, boundHeight)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_create",
                    "returnType": 2
                },
                {
                    "id": "90af3f87-daa1-429c-b855-d37c1cc17142",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_destroy",
                    "help": "ex_camera_destroy(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_destroy",
                    "returnType": 2
                },
                {
                    "id": "497b8e5c-b1d2-4665-b966-f8deaa0fe7d5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_fade",
                    "help": "ex_camera_fade(cameraName, alpha, duration, easing, color, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_fade",
                    "returnType": 2
                },
                {
                    "id": "6edd3e41-a178-4886-bdcb-5cc42b542474",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_fade_in",
                    "help": "ex_camera_fade_in(cameraName, duration, easing, color, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_fade_in",
                    "returnType": 2
                },
                {
                    "id": "417a20be-da21-40df-a8d9-d6e59bbabf68",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_fade_out",
                    "help": "ex_camera_fade_out(cameraName, duration, easing, color, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_fade_out",
                    "returnType": 2
                },
                {
                    "id": "ee7fc8a4-9aae-4689-844c-63e8044b4c23",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_flash",
                    "help": "ex_camera_flash(cameraName, intensity, duration, easing, color, blendMode, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_flash",
                    "returnType": 2
                },
                {
                    "id": "914f99f9-7aaf-46fb-89f4-e80bc957c50b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_debug_mode",
                    "help": "ex_camera_get_debug_mode()",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_debug_mode",
                    "returnType": 2
                },
                {
                    "id": "d30aaa10-d6f8-4334-9476-73faa60d94f6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_fade_color",
                    "help": "ex_camera_get_fade_color(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_fade_color",
                    "returnType": 2
                },
                {
                    "id": "2a7014d7-0d31-444f-a9c3-bb72e65c5c6b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_height",
                    "help": "ex_camera_get_height(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_height",
                    "returnType": 2
                },
                {
                    "id": "30580d91-a773-4d61-bdc4-1c364468a13d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_index",
                    "help": "ex_camera_get_index(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_index",
                    "returnType": 2
                },
                {
                    "id": "f96490f9-e30d-4fe2-aeb5-a197699bc246",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_name",
                    "help": "ex_camera_get_name(cameraIndex)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_name",
                    "returnType": 2
                },
                {
                    "id": "c3d3350e-9ba9-4243-8e98-8f6a14213fc0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_safe_area",
                    "help": "ex_camera_get_safe_area(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_safe_area",
                    "returnType": 2
                },
                {
                    "id": "9237fb99-9ed7-490f-8056-7b7a567558d8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_scroll_position_x",
                    "help": "ex_camera_get_scroll_position_x(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_scroll_position_x",
                    "returnType": 2
                },
                {
                    "id": "4e653be3-6419-47e6-bf1a-37475077cadb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_scroll_position_y",
                    "help": "ex_camera_get_scroll_position_y(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_scroll_position_y",
                    "returnType": 2
                },
                {
                    "id": "dfb9c643-3c13-47d7-81da-d9f64fdde6f3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_tilt_angle",
                    "help": "ex_camera_get_tilt_angle(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_tilt_angle",
                    "returnType": 2
                },
                {
                    "id": "2cd7b707-3a94-418a-a789-9389d89c282b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_view_index",
                    "help": "ex_camera_get_view_index(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_view_index",
                    "returnType": 2
                },
                {
                    "id": "8ecec8db-231c-4edb-90aa-4317d9a79d0a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_width",
                    "help": "ex_camera_get_width(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_width",
                    "returnType": 2
                },
                {
                    "id": "1e671811-eab6-4f7a-8345-c23500b2e0b5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_x",
                    "help": "ex_camera_get_x(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_x",
                    "returnType": 2
                },
                {
                    "id": "aea86627-2cfb-4235-8e52-94e99a785ef7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_y",
                    "help": "ex_camera_get_y(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_y",
                    "returnType": 2
                },
                {
                    "id": "3caf485b-5508-4688-ba43-e908317b5daa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_zoom_offset_x",
                    "help": "ex_camera_get_zoom_offset_x(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_zoom_offset_x",
                    "returnType": 2
                },
                {
                    "id": "88a0f2c8-0bc0-48cd-ad3e-69a6e654b921",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_zoom_offset_y",
                    "help": "ex_camera_get_zoom_offset_y(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_zoom_offset_y",
                    "returnType": 2
                },
                {
                    "id": "5f6a309f-b861-41ad-b3dd-2c4e64d5cc89",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_get_zoom_scale",
                    "help": "ex_camera_get_zoom_scale(cameraName, normalize)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_get_zoom_scale",
                    "returnType": 2
                },
                {
                    "id": "3a4eb82b-c479-4d55-aed6-8bbcb6e63c38",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_initialize",
                    "help": "ex_camera_initialize()",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_initialize",
                    "returnType": 2
                },
                {
                    "id": "903105c5-b40d-4df3-a42b-d15fdadc55bb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_is_shaking",
                    "help": "ex_camera_is_shaking(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_is_shaking",
                    "returnType": 2
                },
                {
                    "id": "037eca06-d49e-4fc0-9513-3765da7c6c07",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_is_visible",
                    "help": "ex_camera_is_visible(cameraName)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_is_visible",
                    "returnType": 2
                },
                {
                    "id": "ae1a9925-82d8-4e34-96e9-adbffdb4e03a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_resize",
                    "help": "ex_camera_resize(cameraName, x, y, width, height, duration, easing, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_resize",
                    "returnType": 2
                },
                {
                    "id": "79a98eec-b267-4d94-bb6d-b0de4d496f19",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_scroll_by",
                    "help": "ex_camera_scroll_by(cameraName, x, y, duration, easing, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_scroll_by",
                    "returnType": 2
                },
                {
                    "id": "d40971dc-e3d5-4c05-a9ea-528918046882",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_scroll_to_object",
                    "help": "ex_camera_scroll_to_object(cameraName, instanceID, duration, easing, center, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_scroll_to_object",
                    "returnType": 2
                },
                {
                    "id": "49f7d350-9474-4991-b4a6-46002e2f5c5f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_scroll_to_point",
                    "help": "ex_camera_scroll_to_point(cameraName, x, y, duration, easing, center, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_scroll_to_point",
                    "returnType": 2
                },
                {
                    "id": "e194fc0a-8655-4756-a637-45b256ee1815",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_bounds",
                    "help": "ex_camera_set_bounds(cameraName, x, y, width, height)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_bounds",
                    "returnType": 2
                },
                {
                    "id": "7251701d-bf66-4514-841b-997a1d2ffb76",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_debug_mode",
                    "help": "ex_camera_set_debug_mode(enabled)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_debug_mode",
                    "returnType": 2
                },
                {
                    "id": "f20e503f-ed12-4492-887f-dacb255c61f1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_fade_alpha",
                    "help": "ex_camera_set_fade_alpha(cameraName, alpha)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_fade_alpha",
                    "returnType": 2
                },
                {
                    "id": "1a3952fb-de29-422e-8e79-c997dccd201b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_fade_color",
                    "help": "ex_camera_set_fade_color(cameraName, color)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_fade_color",
                    "returnType": 2
                },
                {
                    "id": "75a97d9b-0226-4e5a-b91d-7690ce7b333c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_fade_draw_target",
                    "help": "ex_camera_set_fade_draw_target(cameraName, drawTarget)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_fade_draw_target",
                    "returnType": 2
                },
                {
                    "id": "4b1d3177-181b-429a-a8b2-c44192debb16",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_flash_blend",
                    "help": "ex_camera_set_flash_blend(cameraName, blendMode)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_flash_blend",
                    "returnType": 2
                },
                {
                    "id": "70a5ac61-ba8d-4623-b0ff-db3f143acb95",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_flash_color",
                    "help": "ex_camera_set_flash_color(cameraName, color)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_flash_color",
                    "returnType": 2
                },
                {
                    "id": "4d50df0c-9682-47bb-8e4d-806c6ddbba9b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_flash_draw_target",
                    "help": "ex_camera_set_flash_draw_target(cameraName, drawTarget)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_flash_draw_target",
                    "returnType": 2
                },
                {
                    "id": "182da0a8-60e9-4009-af17-f36ac305772e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_position",
                    "help": "ex_camera_set_position(cameraName, x, y)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_position",
                    "returnType": 2
                },
                {
                    "id": "c1ce7f13-bd0b-4a67-aad3-efd1ba3cec39",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_safe_area",
                    "help": "ex_camera_set_safe_area(cameraName, value)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_safe_area",
                    "returnType": 2
                },
                {
                    "id": "42f7a90a-1683-4657-91df-f311f7d6b393",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_scroll_position_at_object",
                    "help": "ex_camera_set_scroll_position_at_object(cameraName, instanceID, center)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_scroll_position_at_object",
                    "returnType": 2
                },
                {
                    "id": "1f214f78-d609-4602-aa8d-ad6fcf516cc4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_scroll_position_at_point",
                    "help": "ex_camera_set_scroll_position_at_point(cameraName, x, y, center)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_scroll_position_at_point",
                    "returnType": 2
                },
                {
                    "id": "6825d279-df93-4628-82d8-6dde661d3998",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_size",
                    "help": "ex_camera_set_size(cameraName, width, height)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_size",
                    "returnType": 2
                },
                {
                    "id": "50291308-7fdd-4049-b841-631083df83ce",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_tilt_angle",
                    "help": "ex_camera_set_tilt_angle(cameraName, value)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_tilt_angle",
                    "returnType": 2
                },
                {
                    "id": "98d87ed2-51dd-4c6c-95d1-2e0c993c7036",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_visible",
                    "help": "ex_camera_set_visible(cameraName, value)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_visible",
                    "returnType": 2
                },
                {
                    "id": "8fedba49-5d55-46be-8b10-7a31458310e8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_set_zoom_scale",
                    "help": "ex_camera_set_zoom_scale(cameraName, scale)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_set_zoom_scale",
                    "returnType": 2
                },
                {
                    "id": "6b80005f-3dbc-40f1-ac4a-42adf9321962",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_shake",
                    "help": "ex_camera_shake(cameraName, shakeX, shakeY, shakeAngle, duration, easing, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_shake",
                    "returnType": 2
                },
                {
                    "id": "c0f91856-bb83-4c65-93ef-75927b437689",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_tilt",
                    "help": "ex_camera_tilt(cameraName, value, duration, easing, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_tilt",
                    "returnType": 2
                },
                {
                    "id": "37c70ae8-0ca7-4cb6-be72-b5b33d444eb7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_zoom",
                    "help": "ex_camera_zoom(cameraName, scale, duration, easing, onComplete, onCompleteArguments)",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_zoom",
                    "returnType": 2
                },
                {
                    "id": "3a3244e8-8c27-46d5-8793-6934c3ee46d3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_math_smoothstep",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_math_smoothstep",
                    "returnType": 2
                },
                {
                    "id": "39d8f559-370e-4e51-8035-bf80a038e2c5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": -1,
                    "args": [
                        
                    ],
                    "externalName": "ex_camera_ds_grid_delete_y",
                    "help": "",
                    "hidden": false,
                    "kind": 11,
                    "name": "ex_camera_ds_grid_delete_y",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "4cf90769-8a81-4fc9-8513-ea1a1d26e582",
                "69fcd511-9d68-49eb-a231-649b17735b5a",
                "683a9808-53b7-4cb9-8627-ed21cf7badde",
                "90af3f87-daa1-429c-b855-d37c1cc17142",
                "497b8e5c-b1d2-4665-b966-f8deaa0fe7d5",
                "6edd3e41-a178-4886-bdcb-5cc42b542474",
                "417a20be-da21-40df-a8d9-d6e59bbabf68",
                "ee7fc8a4-9aae-4689-844c-63e8044b4c23",
                "914f99f9-7aaf-46fb-89f4-e80bc957c50b",
                "d30aaa10-d6f8-4334-9476-73faa60d94f6",
                "2a7014d7-0d31-444f-a9c3-bb72e65c5c6b",
                "30580d91-a773-4d61-bdc4-1c364468a13d",
                "f96490f9-e30d-4fe2-aeb5-a197699bc246",
                "c3d3350e-9ba9-4243-8e98-8f6a14213fc0",
                "9237fb99-9ed7-490f-8056-7b7a567558d8",
                "4e653be3-6419-47e6-bf1a-37475077cadb",
                "dfb9c643-3c13-47d7-81da-d9f64fdde6f3",
                "2cd7b707-3a94-418a-a789-9389d89c282b",
                "8ecec8db-231c-4edb-90aa-4317d9a79d0a",
                "1e671811-eab6-4f7a-8345-c23500b2e0b5",
                "aea86627-2cfb-4235-8e52-94e99a785ef7",
                "3caf485b-5508-4688-ba43-e908317b5daa",
                "88a0f2c8-0bc0-48cd-ad3e-69a6e654b921",
                "5f6a309f-b861-41ad-b3dd-2c4e64d5cc89",
                "3a4eb82b-c479-4d55-aed6-8bbcb6e63c38",
                "903105c5-b40d-4df3-a42b-d15fdadc55bb",
                "037eca06-d49e-4fc0-9513-3765da7c6c07",
                "ae1a9925-82d8-4e34-96e9-adbffdb4e03a",
                "79a98eec-b267-4d94-bb6d-b0de4d496f19",
                "d40971dc-e3d5-4c05-a9ea-528918046882",
                "49f7d350-9474-4991-b4a6-46002e2f5c5f",
                "e194fc0a-8655-4756-a637-45b256ee1815",
                "7251701d-bf66-4514-841b-997a1d2ffb76",
                "f20e503f-ed12-4492-887f-dacb255c61f1",
                "1a3952fb-de29-422e-8e79-c997dccd201b",
                "75a97d9b-0226-4e5a-b91d-7690ce7b333c",
                "4b1d3177-181b-429a-a8b2-c44192debb16",
                "70a5ac61-ba8d-4623-b0ff-db3f143acb95",
                "4d50df0c-9682-47bb-8e4d-806c6ddbba9b",
                "182da0a8-60e9-4009-af17-f36ac305772e",
                "c1ce7f13-bd0b-4a67-aad3-efd1ba3cec39",
                "42f7a90a-1683-4657-91df-f311f7d6b393",
                "1f214f78-d609-4602-aa8d-ad6fcf516cc4",
                "6825d279-df93-4628-82d8-6dde661d3998",
                "50291308-7fdd-4049-b841-631083df83ce",
                "98d87ed2-51dd-4c6c-95d1-2e0c993c7036",
                "8fedba49-5d55-46be-8b10-7a31458310e8",
                "6b80005f-3dbc-40f1-ac4a-42adf9321962",
                "c0f91856-bb83-4c65-93ef-75927b437689",
                "37c70ae8-0ca7-4cb6-be72-b5b33d444eb7",
                "3a3244e8-8c27-46d5-8793-6934c3ee46d3",
                "39d8f559-370e-4e51-8035-bf80a038e2c5"
            ],
            "origname": "extensions\\exCamera.gml",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": null,
    "iosplistinject": "",
    "license": "Free for commercial and non-commercial projects",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.alexandervrs.excamera",
    "productID": "FA34D49C494ABB042A2ED04A27E7F44D",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": null,
    "tvosdelegatename": null,
    "tvosmaccompilerflags": null,
    "tvosmaclinkerflags": null,
    "tvosplistinject": null,
    "version": "1.6.0"
}