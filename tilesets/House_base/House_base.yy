{
    "id": "db9b89ff-9f89-4166-b084-e060cf0fc455",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "House_base",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 1,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "ba4fa60b-1273-48a1-8c4c-c2a83f29175f",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 2,
    "tileheight": 128,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 128,
    "tilexoff": 0,
    "tileyoff": 0
}