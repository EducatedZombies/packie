randomize();
//global.difficulty_base;
//global.hp_base;
//global.damage_base;
//global.shield_base;
//global.maxspeed_base;
//global.acceleration_base;

//global.turretsize;

//global.bulletdamage;

//global.player_name = Annonymous;
global.gr_friction = 0.01;              //Base value of friction for current level.

global.startdir = 45;                 //Starting angle
global.startposx = 1280;                //Start position X
global.startposy = 1500;                //Start position Y

global.tankacc = 1;                  //Tank acceleration speed, 1 = +1 pixel per frame
global.tankmaxspeed = 7;               //Tank top speed, 1 = 1 pixel per frame
global.tanktype = spr_tank_box;        //****what sprite the tank has
global.tanksize =0.2;                  //Multiplyer of the tank size
global.tankcolor = -1;                 //**** set tank ground color
global.tankrotate = 1;                 //Tank rotate speed, 1 = 1 degree per frame


global.turretspeed = 2;                //Rotating speed of the turret, 1 = 1 degree per frame
global.turrettype = spr_turret_box;      //**** what sprite the turret has
global.turretmax = 20;                 //Maximum rotate angle of the turret

global.bulletfirerate = 10;            //fire rate bullets. 10 = one every 10 frame.
global.bullettype = spr_laser;         //**** what sprite the bullet has
global.bulletsize = 1;                 //****muliplyer of the bullet size
global.bulletspeed = 12;                //Speed of the bullet, 1 = 1 pixel per frame

instance_create_layer((global.startposx), (global.startposy), "lyr_tank", obj_tank_body);

instance_create_layer((random_range(0,room_width)), 100, "lyr_objects", obj_enemy_blob);
