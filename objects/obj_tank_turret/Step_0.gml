image_angle = direction;
turrdiff = angle_difference(obj_tank_turret.direction, obj_tank_body.direction);
fireratecounter = fireratecounter+1;
if (keyboard_check (vk_left))
{
    
    if (turrdiff <= global.turretmax)
    {
        direction +=global.turretspeed;
    }
}   
 
if (keyboard_check (vk_right)) 
{
    if (turrdiff >=-global.turretmax)
    {
        direction -=global.turretspeed;
    }
}
if (keyboard_check (vk_up))
{
    if fireratecounter >= global.bulletfirerate
    {
        instance_create_layer(x + lengthdir_x(turretlength, image_angle), y + lengthdir_y(turretlength, image_angle), "lyr_objects", obj_bullet);
        fireratecounter=0;
    }
}

