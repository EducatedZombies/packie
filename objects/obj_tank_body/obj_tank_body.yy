{
    "id": "884edd2e-ecc0-48a1-8144-a00e8d10ad6a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tank_body",
    "eventList": [
        {
            "id": "b1214320-258d-415c-812c-7364b0e3915a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "884edd2e-ecc0-48a1-8144-a00e8d10ad6a"
        },
        {
            "id": "75f641e3-1c72-4dbd-8f15-30c35fe18a46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "884edd2e-ecc0-48a1-8144-a00e8d10ad6a"
        },
        {
            "id": "3e3ece9d-bb81-441b-bf33-0c100b97ecc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "884edd2e-ecc0-48a1-8144-a00e8d10ad6a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}