{
    "id": "8fc04f6e-20bb-4679-a0f6-89abd32ba8d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tile_city_roads",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9223cbc6-f826-4836-b5c0-c5e0cc2b859f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc04f6e-20bb-4679-a0f6-89abd32ba8d6",
            "compositeImage": {
                "id": "bed4c799-d0e2-4b56-a8c1-7514994dae59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9223cbc6-f826-4836-b5c0-c5e0cc2b859f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d66b6c-3285-4f11-ab9a-775a1d5ad015",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9223cbc6-f826-4836-b5c0-c5e0cc2b859f",
                    "LayerId": "db1a5574-32a0-4ea0-890d-37a76b3bee06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "db1a5574-32a0-4ea0-890d-37a76b3bee06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fc04f6e-20bb-4679-a0f6-89abd32ba8d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}