{
    "id": "fcdffd52-2d92-4755-b5f6-83369a3e158d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "774555e0-c5e6-4221-b055-ed97da3d3368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcdffd52-2d92-4755-b5f6-83369a3e158d",
            "compositeImage": {
                "id": "690fd483-1a87-4345-867c-e7db8879c26d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "774555e0-c5e6-4221-b055-ed97da3d3368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8024c295-d0f9-4eff-a66d-a4ac4b0d1278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "774555e0-c5e6-4221-b055-ed97da3d3368",
                    "LayerId": "4d44a846-bb3b-4ce9-a53d-fe62a58c7f84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "4d44a846-bb3b-4ce9-a53d-fe62a58c7f84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcdffd52-2d92-4755-b5f6-83369a3e158d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 0,
    "yorig": 4
}