{
    "id": "93156ef8-4122-4899-b1af-0b92def4a149",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 123,
    "bbox_left": 0,
    "bbox_right": 123,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0aa84fa-ce0a-4e67-9225-cbbf2de9c5eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93156ef8-4122-4899-b1af-0b92def4a149",
            "compositeImage": {
                "id": "53ad058f-6332-4703-afd3-630a8cf0f905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0aa84fa-ce0a-4e67-9225-cbbf2de9c5eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "755cd61c-d47a-4499-bfff-95f58ff44c54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0aa84fa-ce0a-4e67-9225-cbbf2de9c5eb",
                    "LayerId": "b02795d1-97f3-4a2b-a74d-80556bb3f903"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "b02795d1-97f3-4a2b-a74d-80556bb3f903",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93156ef8-4122-4899-b1af-0b92def4a149",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 63,
    "yorig": 63
}