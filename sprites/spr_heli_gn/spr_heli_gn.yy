{
    "id": "a928120c-1213-4152-8de9-557410bb97f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heli_gn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 288,
    "bbox_left": 64,
    "bbox_right": 250,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4373ec38-36be-4807-bfa6-60cb3507ed0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a928120c-1213-4152-8de9-557410bb97f9",
            "compositeImage": {
                "id": "80dc3dd2-14a1-4328-a04a-af30a2777da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4373ec38-36be-4807-bfa6-60cb3507ed0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "004553d5-1999-46f1-9378-23e1a44ad0bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4373ec38-36be-4807-bfa6-60cb3507ed0b",
                    "LayerId": "89460371-4832-400a-927d-2f31deead347"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "89460371-4832-400a-927d-2f31deead347",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a928120c-1213-4152-8de9-557410bb97f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}