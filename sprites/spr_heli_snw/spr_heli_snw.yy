{
    "id": "f084f3f6-33a7-46c9-8e55-95cafdb502af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heli_snw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 288,
    "bbox_left": 64,
    "bbox_right": 250,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ed3644d-5970-4cd3-b73a-0c75d33b4766",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f084f3f6-33a7-46c9-8e55-95cafdb502af",
            "compositeImage": {
                "id": "2fa44040-5046-41a4-93f8-31d5856c44cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ed3644d-5970-4cd3-b73a-0c75d33b4766",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e32cef-b9d4-440f-a347-da5b35db23aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ed3644d-5970-4cd3-b73a-0c75d33b4766",
                    "LayerId": "475ac9aa-5533-472c-9e9c-8651ac2eced0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "475ac9aa-5533-472c-9e9c-8651ac2eced0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f084f3f6-33a7-46c9-8e55-95cafdb502af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}