{
    "id": "283bb8cf-a65b-4fd2-96e7-535bfaacd143",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tank_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "048e13cf-da1f-4e69-8b66-cf2b31d72e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "283bb8cf-a65b-4fd2-96e7-535bfaacd143",
            "compositeImage": {
                "id": "6198393b-e5c6-4e90-8962-c4fe14a4c6ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048e13cf-da1f-4e69-8b66-cf2b31d72e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8354758-4c11-466c-b9ea-77ce7d7d2522",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048e13cf-da1f-4e69-8b66-cf2b31d72e52",
                    "LayerId": "f74302a0-30db-4b6e-883f-734549fbb95e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "f74302a0-30db-4b6e-883f-734549fbb95e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "283bb8cf-a65b-4fd2-96e7-535bfaacd143",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 6
}