{
    "id": "16440190-d225-41e4-805f-3a1280a7e746",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heli_rtr_dft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 233,
    "bbox_left": 43,
    "bbox_right": 258,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1485d15-7077-46e2-82a4-beccc3fc619d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16440190-d225-41e4-805f-3a1280a7e746",
            "compositeImage": {
                "id": "501b6475-d444-4b33-94ae-e3510009b78c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1485d15-7077-46e2-82a4-beccc3fc619d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c0437b2-dd40-48df-b565-d9eb3c74db2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1485d15-7077-46e2-82a4-beccc3fc619d",
                    "LayerId": "c278d5f8-17c9-433c-ba20-f19472f5eb71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "c278d5f8-17c9-433c-ba20-f19472f5eb71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16440190-d225-41e4-805f-3a1280a7e746",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 0,
    "yorig": 0
}