{
    "id": "b5ea723c-6c7a-4807-8efb-643fbc0a0223",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tank_testturret",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55fd62e9-5682-4895-b128-e696877a4c83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5ea723c-6c7a-4807-8efb-643fbc0a0223",
            "compositeImage": {
                "id": "122826ae-84ed-4682-bdea-5fd66fd94c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55fd62e9-5682-4895-b128-e696877a4c83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9b1703-d514-4bad-96b0-c35a525418e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55fd62e9-5682-4895-b128-e696877a4c83",
                    "LayerId": "1d242f18-d255-4ff1-8604-cb89144a1c26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "1d242f18-d255-4ff1-8604-cb89144a1c26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5ea723c-6c7a-4807-8efb-643fbc0a0223",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 4,
    "yorig": 4
}