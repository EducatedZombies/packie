{
    "id": "6bd0ea75-4f77-4923-b8a0-ce5325483a2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tank_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 124,
    "bbox_left": 2,
    "bbox_right": 122,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aa82bb5-c5ae-434c-bdfe-3e3cc90d7441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6bd0ea75-4f77-4923-b8a0-ce5325483a2f",
            "compositeImage": {
                "id": "0457e19e-8e99-4d4f-ad08-5341cf9b866b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa82bb5-c5ae-434c-bdfe-3e3cc90d7441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13fa77f0-f8c8-4e08-a77f-003cbc833079",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa82bb5-c5ae-434c-bdfe-3e3cc90d7441",
                    "LayerId": "c3347ddc-97c7-4345-b4a9-10ce8f313880"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "c3347ddc-97c7-4345-b4a9-10ce8f313880",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6bd0ea75-4f77-4923-b8a0-ce5325483a2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 62
}