{
    "id": "ba4fa60b-1273-48a1-8c4c-c2a83f29175f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 121,
    "bbox_left": 136,
    "bbox_right": 249,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5368278-e99d-49a0-8e3f-55c20ea9094c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba4fa60b-1273-48a1-8c4c-c2a83f29175f",
            "compositeImage": {
                "id": "5ed0e3f5-b1dd-42ef-854e-898351b45592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5368278-e99d-49a0-8e3f-55c20ea9094c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca531ebf-b061-4812-9e83-a38d1671845f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5368278-e99d-49a0-8e3f-55c20ea9094c",
                    "LayerId": "ab6790cd-cc0f-4ebc-b6a9-1b8e1107d294"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ab6790cd-cc0f-4ebc-b6a9-1b8e1107d294",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba4fa60b-1273-48a1-8c4c-c2a83f29175f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": -80,
    "yorig": -120
}