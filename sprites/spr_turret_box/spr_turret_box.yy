{
    "id": "2ea774cb-d024-4bcd-a634-ff92910dc73b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_turret_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 25,
    "bbox_right": 159,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e25ccfc2-32bd-443d-a632-b0a677d8b076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea774cb-d024-4bcd-a634-ff92910dc73b",
            "compositeImage": {
                "id": "f2161ef1-31ac-4a0b-805a-e4469cd81f15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25ccfc2-32bd-443d-a632-b0a677d8b076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f730de2-a600-436d-9e70-373f586d0b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25ccfc2-32bd-443d-a632-b0a677d8b076",
                    "LayerId": "eabec2a7-8053-469c-898f-6ecb766839b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 125,
    "layers": [
        {
            "id": "eabec2a7-8053-469c-898f-6ecb766839b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ea774cb-d024-4bcd-a634-ff92910dc73b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 62,
    "yorig": 62
}