{
    "id": "3a117945-6cf9-43b2-9953-23c04fcb35b0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_main",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Century Gothic",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "90897ead-f22c-4953-ad5d-1ec38d181041",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 78,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 130,
                "y": 402
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2f8ce25f-87c8-45d3-97ec-494a94a64cc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 66,
                "offset": 5,
                "shift": 19,
                "w": 9,
                "x": 409,
                "y": 402
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6fd92843-93f6-47e0-8710-f32beee3051a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 431,
                "y": 402
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "35487bd0-f61d-457c-ba09-92b2b8f02db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 65,
                "offset": 5,
                "shift": 46,
                "w": 36,
                "x": 231,
                "y": 162
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a4efd7c4-6c64-4e74-985e-eec8b6dd811b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 76,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 449,
                "y": 82
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "20c84a23-1af7-4282-8139-9b6e41138b34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 68,
                "offset": 1,
                "shift": 50,
                "w": 48,
                "x": 328,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "79420771-b6c7-44bb-961a-7b43abddf20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 66,
                "offset": 5,
                "shift": 48,
                "w": 42,
                "x": 212,
                "y": 82
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cfa3664c-bd87-41b2-89c6-f77ee5f78e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 489,
                "y": 402
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "af52f287-fc16-4d51-b577-46f0456dd5ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 78,
                "offset": 8,
                "shift": 24,
                "w": 12,
                "x": 263,
                "y": 402
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "74e81113-fd70-464c-8937-30955e155ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 78,
                "offset": 3,
                "shift": 24,
                "w": 13,
                "x": 233,
                "y": 402
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "62b0d2c7-126e-4018-ba9b-b87ab379d89b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 40,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 291,
                "y": 402
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "61ac47e9-99fa-4f30-9478-2a99322ca044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 58,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 134,
                "y": 322
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "fb2bfcb5-9fe7-4901-813b-96898d22f619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 71,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 363,
                "y": 402
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "05d032d9-d2ef-4c37-8907-aef7220204fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 50,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 316,
                "y": 402
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "66c73e70-be16-4ef3-9898-e9b9b8cce15f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 66,
                "offset": 5,
                "shift": 18,
                "w": 9,
                "x": 387,
                "y": 402
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a3c29c43-7af1-498b-8509-1e0bc0808716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 71,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 351,
                "y": 322
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4048b29a-0561-4ccd-989a-3549a8bb87e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 66,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 37,
                "y": 242
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7ff6dc98-3ea8-49f1-9631-a630d4637f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 65,
                "offset": 9,
                "shift": 35,
                "w": 12,
                "x": 349,
                "y": 402
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cae000d7-3d99-448b-bd04-b7340a1026fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 65,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "85ae331a-d1f3-4c4f-9df5-14858c05eb0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 66,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 240,
                "y": 242
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "26ad587e-3085-4cac-b940-2478ec5397f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 65,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 451,
                "y": 162
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5742846d-4002-466f-99e5-bfb2c16c84be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 66,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 273,
                "y": 242
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b8c0efdb-7bf9-470f-9ec6-1600d759a268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 66,
                "offset": 4,
                "shift": 35,
                "w": 29,
                "x": 72,
                "y": 322
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "005925d5-112f-446f-8f61-7d63837685e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 66,
                "offset": 4,
                "shift": 35,
                "w": 30,
                "x": 444,
                "y": 242
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f3d3fa29-d0e2-4850-8650-9f8f3fc6bdc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 66,
                "offset": 2,
                "shift": 35,
                "w": 32,
                "x": 105,
                "y": 242
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9dc91ba6-c782-4700-aa24-01c1961b0aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 66,
                "offset": 3,
                "shift": 35,
                "w": 29,
                "x": 103,
                "y": 322
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "51504600-949c-435d-a9fd-30233c1965b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 66,
                "offset": 5,
                "shift": 18,
                "w": 9,
                "x": 398,
                "y": 402
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "17fe1e0f-3ee1-4b30-bbd1-0348d2a04a27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 71,
                "offset": 4,
                "shift": 18,
                "w": 10,
                "x": 375,
                "y": 402
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "11ea88eb-ca67-44a1-816f-dea9762d5bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 59,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 37,
                "y": 322
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1ca0b03d-0381-427a-9204-17fd8f09609b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 51,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 435,
                "y": 322
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f8aeb865-8bb9-4aae-bff9-cea5a439f757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 59,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 2,
                "y": 322
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "418f5911-4241-4dc9-b30e-8357ee2acb30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 66,
                "offset": 5,
                "shift": 38,
                "w": 28,
                "x": 261,
                "y": 322
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a9ebb31b-d8b6-45ad-a088-8d1c486032b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 66,
                "offset": 4,
                "shift": 56,
                "w": 48,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c717b711-6852-4aca-bbbd-536cbbead677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 65,
                "offset": 1,
                "shift": 47,
                "w": 45,
                "x": 50,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4a95adeb-74c1-4271-9879-0a5f861ccd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 65,
                "offset": 5,
                "shift": 37,
                "w": 29,
                "x": 200,
                "y": 322
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "109893eb-a2ae-4255-9dba-6f603f705c4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 66,
                "offset": 3,
                "shift": 52,
                "w": 46,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "928e3657-542a-4dde-bfc4-17dab132cf01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 65,
                "offset": 5,
                "shift": 48,
                "w": 40,
                "x": 299,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e02e985e-14df-40f5-b34c-7d5645b61d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 65,
                "offset": 5,
                "shift": 34,
                "w": 27,
                "x": 378,
                "y": 322
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c0a1c2d0-785e-4fd4-9f89-ba68781c1fe0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 65,
                "offset": 5,
                "shift": 31,
                "w": 24,
                "x": 2,
                "y": 402
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c84fd460-e3ea-47d1-93eb-b6a6648a1772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 66,
                "offset": 3,
                "shift": 56,
                "w": 50,
                "x": 276,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f4fa0fe6-2b77-476a-a03d-62972d2e229b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 65,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 380,
                "y": 162
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d66592a4-6cfa-4333-9f42-696211453ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 65,
                "offset": 4,
                "shift": 14,
                "w": 6,
                "x": 451,
                "y": 402
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a1a61805-cfc7-4169-802e-4f7d55439521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 66,
                "offset": 2,
                "shift": 31,
                "w": 24,
                "x": 470,
                "y": 322
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c99afb58-8a6d-4487-bb67-09737b755302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 65,
                "offset": 5,
                "shift": 38,
                "w": 32,
                "x": 173,
                "y": 242
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a73a620a-2b65-430a-99d6-558a83a2fc35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 65,
                "offset": 5,
                "shift": 30,
                "w": 23,
                "x": 63,
                "y": 402
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e04cc68d-0783-4a45-8cb4-8388fe07ef4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 65,
                "offset": 3,
                "shift": 59,
                "w": 53,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "effe1e07-fdc0-4c50-818d-82850e78389a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 65,
                "offset": 5,
                "shift": 47,
                "w": 37,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f2f3fd89-db51-46a1-8a2a-ac96b063f600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 66,
                "offset": 3,
                "shift": 56,
                "w": 49,
                "x": 378,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f2a03391-ce31-444a-b119-f1d0d5e965f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 65,
                "offset": 5,
                "shift": 38,
                "w": 30,
                "x": 476,
                "y": 242
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "078fb9a7-5574-49bc-9aba-f039b4b9afcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 68,
                "offset": 3,
                "shift": 56,
                "w": 50,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "13fd2e3f-1f8c-4086-a179-4580adff53f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 65,
                "offset": 5,
                "shift": 39,
                "w": 31,
                "x": 372,
                "y": 242
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a63c8e4d-ead2-4cd0-8099-b6d7f88bf7a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 66,
                "offset": 0,
                "shift": 32,
                "w": 28,
                "x": 231,
                "y": 322
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1b04bdf1-6f0f-4269-b919-13d12fe2d14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 65,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 407,
                "y": 322
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "461d6cad-6ef1-46c3-9095-91a3170a6450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 66,
                "offset": 5,
                "shift": 42,
                "w": 32,
                "x": 71,
                "y": 242
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "29786670-9000-42ca-862f-75c6db3d98d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 65,
                "offset": 2,
                "shift": 45,
                "w": 41,
                "x": 256,
                "y": 82
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4311cf29-324d-48af-a8ed-bf3dfac93124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 65,
                "offset": 3,
                "shift": 61,
                "w": 56,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f27998e2-57ff-490c-af7d-7fec513998d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 65,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 306,
                "y": 162
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0270ee9c-b4d9-4f8b-b623-af70f9d8fa3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 65,
                "offset": 1,
                "shift": 38,
                "w": 35,
                "x": 343,
                "y": 162
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b38e3648-5d85-4f20-a41e-268d770dd3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 65,
                "offset": 1,
                "shift": 31,
                "w": 28,
                "x": 321,
                "y": 322
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b5c087fa-4a08-4921-b83e-eaefaaf0bcf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 77,
                "offset": 9,
                "shift": 22,
                "w": 11,
                "x": 336,
                "y": 402
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "53436712-348e-4d84-903d-4665659af6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 71,
                "offset": 2,
                "shift": 39,
                "w": 35,
                "x": 376,
                "y": 82
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "064ed3d8-7fe2-4cf9-b0df-ec5a94733b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 77,
                "offset": 2,
                "shift": 22,
                "w": 12,
                "x": 277,
                "y": 402
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e55bbb33-e3f2-440e-9e71-dfea48b47004",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 54,
                "offset": 3,
                "shift": 43,
                "w": 37,
                "x": 405,
                "y": 242
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "54108191-dbdf-43c1-bb35-f7521353d7da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 73,
                "offset": -1,
                "shift": 32,
                "w": 34,
                "x": 413,
                "y": 82
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c7b2840c-dc43-4752-9f76-59b2c9550de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 3,
                "shift": 24,
                "w": 14,
                "x": 466,
                "y": 402
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9dcf6d41-1529-4fa3-ad87-583224470477",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 66,
                "offset": 3,
                "shift": 44,
                "w": 36,
                "x": 41,
                "y": 162
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1514d7bf-1016-4973-96bc-1a6d205404c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 66,
                "offset": 5,
                "shift": 44,
                "w": 36,
                "x": 193,
                "y": 162
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "81d4003a-baa7-4674-8215-74412a455570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 66,
                "offset": 3,
                "shift": 41,
                "w": 35,
                "x": 269,
                "y": 162
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "51242e19-42b6-4b73-a6a9-b6488d18d814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 66,
                "offset": 3,
                "shift": 44,
                "w": 36,
                "x": 79,
                "y": 162
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0397526b-3e66-4295-a518-2f92cdeb2b7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 66,
                "offset": 3,
                "shift": 42,
                "w": 36,
                "x": 155,
                "y": 162
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ce52bdcb-a8bb-4f7d-88ba-3629d7d2f513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 65,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 214,
                "y": 402
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5869b934-4bc5-4fd9-8e5e-270660c27349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 78,
                "offset": 3,
                "shift": 43,
                "w": 36,
                "x": 136,
                "y": 82
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f95e978f-c983-4167-9297-5db8af54bc7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 65,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 339,
                "y": 242
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "33108eff-51a7-41f3-8106-93a6c5da6b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 65,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 420,
                "y": 402
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "09b595c4-6d6e-4d99-a203-c27d421abf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 78,
                "offset": -2,
                "shift": 13,
                "w": 13,
                "x": 248,
                "y": 402
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "44e5797a-8201-4a39-975a-b107b879b74a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 65,
                "offset": 4,
                "shift": 32,
                "w": 29,
                "x": 169,
                "y": 322
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "61f393f6-a725-41b9-aeac-e143be5ad861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 65,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 482,
                "y": 402
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "84b5005c-b5ff-4fc6-808f-dcf4d81d7c2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 65,
                "offset": 4,
                "shift": 60,
                "w": 52,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "604cf3ba-39a8-45aa-875c-5638303cd689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 65,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 306,
                "y": 242
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "49c93289-a325-40e1-9fbc-e138a09d5cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 66,
                "offset": 3,
                "shift": 42,
                "w": 36,
                "x": 117,
                "y": 162
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b997b6cb-ac14-4f69-870c-3f7c09969ea6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 77,
                "offset": 4,
                "shift": 44,
                "w": 37,
                "x": 97,
                "y": 82
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "caadff13-eee9-4fa3-8ac9-67c244449fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 77,
                "offset": 3,
                "shift": 44,
                "w": 36,
                "x": 174,
                "y": 82
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a27b9fcf-4a7e-4db7-8485-f58ab6dcf492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 65,
                "offset": 3,
                "shift": 19,
                "w": 18,
                "x": 194,
                "y": 402
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ce490104-a7e1-4fb6-8511-e0dab36dba5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 66,
                "offset": 1,
                "shift": 25,
                "w": 21,
                "x": 150,
                "y": 402
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "eca66a63-5c08-49c1-ad31-b3e431321f7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 65,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 173,
                "y": 402
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0501b3ac-167a-44d3-a86b-dc2d284930c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 66,
                "offset": 4,
                "shift": 39,
                "w": 31,
                "x": 207,
                "y": 242
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3c147e92-fceb-4c90-a4e3-62d9d2079208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 65,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 416,
                "y": 162
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e0b4f36b-9b30-4438-b0bc-8fc55e144540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 65,
                "offset": 0,
                "shift": 53,
                "w": 53,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6ab92bd9-61ff-4c08-9a26-8172c6083a58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 65,
                "offset": -1,
                "shift": 31,
                "w": 32,
                "x": 139,
                "y": 242
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6a38a830-bc85-4123-91ea-14206b546e4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 77,
                "offset": 1,
                "shift": 34,
                "w": 33,
                "x": 341,
                "y": 82
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9eb3b91b-416f-4b8a-acb6-103dbe45c7ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 65,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 291,
                "y": 322
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "142ba2de-922d-4c9d-92e1-a67e11e19a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 78,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 88,
                "y": 402
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "a7897d66-155c-4732-8e6e-e39ea0bf8e08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 77,
                "offset": 19,
                "shift": 43,
                "w": 5,
                "x": 459,
                "y": 402
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "8e0ebb36-f9a5-42fd-8040-811b21e8aa2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 78,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 109,
                "y": 402
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9d3678e1-8319-48fd-9b4b-c657717d717e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 46,
                "offset": 3,
                "shift": 39,
                "w": 33,
                "x": 28,
                "y": 402
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}